## Menu Link Content Fields

This is an incredibly simply module which makes menu link content
entities fieldable. It is now possible to add multiple fields of
any type to custom menu links. These can then be rendered as needed
within the theme to provide functionality such as sub titles, use
selectable colours, or any other data.

### Usage

* Enable the module as normal.
* Navigate to /admin/structure/menu.
* Here you will have three new tabs, Manage fields,
Manage form display, and Manage display.
* Add new form fields via the "Manage fields" tab.


### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
